import axios from "axios";
import environment from "../config/environment.js";

const {
  REGISTRO_CIVIL_API: { BASE_URL, APIKEY },
} = environment;

/**
 * Create an instance of axios with the base URL and headers containing the API key
 * @returns {object} - Axios instance
 */
function createAxiosInstance() {
  return axios.create({
    baseURL: BASE_URL,
    headers: {
      apikey: APIKEY,
    },
  });
}

/**
 * Get criminal records for a person by their RUT (national identification number)
 * @param {string} rut - RUT of the person
 * @param {object} axiosInstance - Optional axios instance to use for the API request
 * @returns {Promise} - Promise that resolves with the API response
 */
async function getCriminalRecords(rut, axiosInstance = createAxiosInstance()) {
  return axiosInstance
    .get(`/person/${rut}/criminal_records`)
    .then((response) => response.data)
    .catch((error) => {
      // Handle errors if needed
      throw error;
    });
}

export { getCriminalRecords };
