import messages from "../../src/messages/verify.messages";
import { required } from "../../src/messages/common.messages";

// Mock de la función 'required'
jest.mock("../../src/messages/common.messages", () => ({
  required: jest.fn((key) => `required: ${key}`),
}));

describe("Verification Messages", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("should return the correct validation error message", () => {
    expect(messages.validation.name).toBe("verify_validation_error");
    expect(messages.validation.messages.code).toBe("required: code");
    expect(required).toHaveBeenCalledWith("code");
  });

  it("should have the correct codeExpired error message", () => {
    expect(messages.codeExpired.name).toBe("verify_code_expired_error");
    expect(messages.codeExpired.message).toBe("the code has expired");
  });

  it("should have the correct userNotFound error message", () => {
    expect(messages.userNotFound.name).toBe("verify_user_not_found_error");
    expect(messages.userNotFound.message).toBe("user not found");
  });

  it("should have the correct codeNotFound error message", () => {
    expect(messages.codeNotFound.name).toBe("verify_code_not_found_error");
    expect(messages.codeNotFound.message).toBe(
      "code not found. Please contact support"
    );
  });

  it("should have the correct invalidCode error message", () => {
    expect(messages.invalidCode.name).toBe("verify_invalid_code_error");
    expect(messages.invalidCode.message).toBe("the code are invalid");
  });

  it("should have the correct alreadyVerified error message", () => {
    expect(messages.alreadyVerified.name).toBe("verify_already_verified_error");
    expect(messages.alreadyVerified.message).toBe("the user is already verified");
  });
});