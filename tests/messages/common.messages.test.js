import { required } from '../../src/messages/common.messages';

describe('Unit Test - required', () => {
  it('should return the correct required message', () => {
    const key = 'name';
    const result = required(key);
    expect(result).toBe('name is required');
  });
});