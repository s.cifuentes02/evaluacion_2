import { JsonWebTokenError } from "jsonwebtoken";
import {
  HTTPError,
  returnErrorResponse,
  isBusinessError,
} from "../../src/helpers/error.helper";

// Mock Response Object
const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnThis();
  res.json = jest.fn().mockReturnThis();
  return res;
};

describe("HTTPError", () => {
  it("should create an instance of HTTPError", () => {
    const errorData = {
      name: "CustomError",
      msg: "Custom error message",
      code: 400,
    };
    const error = new HTTPError(errorData);
    expect(error).toBeInstanceOf(HTTPError);
    expect(error.name).toBe(errorData.name);
    expect(error.msg).toBe(errorData.msg);
    expect(error.statusCode).toBe(errorData.code);
  });
});

describe("returnErrorResponse", () => {
  it("should return HTTPError response if error is an instance of HTTPError", () => {
    const error = new HTTPError({
      name: "CustomError",
      msg: "Custom error message",
      code: 400,
    });
    const res = mockResponse();
    returnErrorResponse(error, res);
    expect(res.status).toBeCalledWith(error.statusCode);
    expect(res.json).toBeCalledWith({ error: { ...error } });
  });

  it("should return JWTError response if error is an instance of JsonWebTokenError", () => {
    const jwtError = new JsonWebTokenError("JWT verification failed");
    const res = mockResponse();
    returnErrorResponse(jwtError, res);
    const expectedError = new HTTPError({
      name: jwtError.name,
      msg: jwtError.message,
      code: 403,
    });
    expect(res.status).toBeCalledWith(403);
    expect(res.json).toBeCalledWith({ error: { ...expectedError } });
  });

  it("should return a generic error response if error is not an instance of HTTPError or JsonWebTokenError", () => {
    const error = new Error("Some error occurred");
    const res = mockResponse();
    returnErrorResponse(error, res);
    expect(res.status).toBeCalledWith(500);
    expect(res.json).toBeCalledWith({ error: error.toString() });
  });
});

describe("isBusinessError", () => {
  it("should return true if error has a statusCode starting with '4'", () => {
    const error = { statusCode: 400 };
    const result = isBusinessError(error);
    expect(result).toBe(true);
  });

  it("should return false if error does not have a statusCode starting with '4'", () => {
    const error = { statusCode: 500 };
    const result = isBusinessError(error);
    expect(result).toBe(false);
  });

  it("should return false if error does not have a statusCode property", () => {
    const error = { message: "Some error" };
    const result = isBusinessError(error);
    expect(result).toBe(undefined);
  });
});