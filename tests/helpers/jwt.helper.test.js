import jwt from 'jsonwebtoken';
import { generateToken, verifyToken } from '../../src/helpers/jwt.helper';


// Mock del módulo environment
jest.mock('../../src/config/environment', () => ({
  JWT: {
    SECRET: 'my-secret-key',
    DEFAULT_EXPIRES: '1h',
  },
}));

describe('JWT Tests', () => {
  const data = { id: 1, name: 'John Doe' };
  const token = 'generated-token';

  // Mock para jwt.sign
  jwt.sign = jest.fn().mockReturnValue(token);

  // Mock para jwt.verify
  jwt.verify = jest.fn().mockReturnValue(data);

  it('should generate a valid JWT token', () => {
    const result = generateToken({ data });
    expect(result).toBe(token);
    expect(jwt.sign).toHaveBeenCalledWith(data, 'my-secret-key', { expiresIn: '1h' });
  });

  it('should verify and decode a JWT token', () => {
    const result = verifyToken(token);
    expect(result).toEqual(data);
    expect(jwt.verify).toHaveBeenCalledWith(token, 'my-secret-key');
  });
});