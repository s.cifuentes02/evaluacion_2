import email from "../../src/emails/validate.email";

describe("email", () => {
  it("should return the correct email template", () => {
    const name = "Saul";
    const code = "ABC123";
    
    const expectedEmail =
      `<h1> Hola ${name}! </h1>` +
      "<p> Necesitas validar este email para unirte a la plataforma </p>" +
      `<p> Usa este código en la aplicación: ${code} </p>` +
      `<p> <strong>¡Respeta las mayúsculas y minúsculas!</strong> </p>` +
      `<br><p> Un saludo :) </p>`;
      
    const result = email({ name, code });
    
    expect(result).toEqual(expectedEmail);
  });
});