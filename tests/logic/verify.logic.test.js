import { HTTPError } from "../../src/helpers/error.helper.js";
import verifyMessages from "../../src/messages/verify.messages.js";
import { verifyToken } from "../../src/helpers/jwt.helper.js";
import { extractCode, checkIfUserIsAlreadyVerified } from "../../src/logic/verify.logic.js";

// Mock para la función verifyToken
jest.mock("../../src/helpers/jwt.helper.js", () => ({
    verifyToken: jest.fn(),
}));

// Mock para la clase HTTPError
jest.mock("../../src/helpers/error.helper.js", () => ({
    HTTPError: jest.fn(),
}));

describe("extractCode", () => {
    it("should extract the code from a JWT token", () => {
        // Datos de prueba
        const token = "exampleToken";
        const decodedToken = {
            code: "exampleCode",
        };

        // Mock de la función verifyToken
        verifyToken.mockReturnValue(decodedToken);

        // Llamada al método extractCode
        const result = extractCode({ code: token });

        // Verificar que la función verifyToken haya sido llamada con los argumentos correctos
        expect(verifyToken).toHaveBeenCalledWith(token);

        // Verificar que el resultado sea el código extraído del token
        expect(result).toBe(decodedToken.code);
    });

    it("should throw an HTTPError with codeExpired details if verifyToken throws an error", () => {
        // Mock de la función verifyToken para que lance un error
        verifyToken.mockImplementation(() => {
            throw new Error("Token verification error");
        });

        // Mock de la clase HTTPError
        const mockHTTPError = jest.fn();
        mockHTTPError.prototype.name = verifyMessages.codeExpired.name;
        mockHTTPError.prototype.msg = verifyMessages.codeExpired.message;
        mockHTTPError.prototype.code = 400;
        HTTPError.mockImplementation(mockHTTPError);

        // Llamada al método extractCode dentro de un bloque try-catch
        try {
            extractCode({ code: "invalidToken" });
            // Si no se lanza una excepción, la prueba debe fallar
            expect(true).toBe(false);
        } catch (error) {
            // Verificar que se haya lanzado un HTTPError con los detalles correctos
            expect(HTTPError).toHaveBeenCalled();
            expect(HTTPError).toHaveBeenCalledWith({
                name: verifyMessages.codeExpired.name,
                msg: verifyMessages.codeExpired.message,
                code: 400,
            });
        }
    });
});
describe("checkIfUserIsAlreadyVerified", () => {
    it("should throw an HTTPError if user is already verified", () => {
        // Mock de la clase HTTPError
        HTTPError.mockImplementation((errorDetails) => ({
            ...errorDetails,
            toJSON: jest.fn().mockReturnValue(errorDetails),
        }));

        // Crear un objeto de usuario verificado
        const user = {
            verified: true,
        };

        // Llamada al método checkIfUserIsAlreadyVerified dentro de un bloque try-catch
        try {
            checkIfUserIsAlreadyVerified(user);
            // Si no se lanza una excepción, la prueba debe fallar
            expect(true).toBe(false);
        } catch (error) {
            // Verificar que se haya lanzado un HTTPError con los detalles correctos
            expect(HTTPError).toHaveBeenCalledWith({
                name: verifyMessages.alreadyVerified.name,
                msg: verifyMessages.alreadyVerified.message,
                code: 400,
            });

        }
    });

    it("should not throw an HTTPError if user is not verified", () => {
        // Datos de prueba
        const unverifiedUser = {
            verified: false,
        };

        // Llamada al método checkIfUserIsAlreadyVerified
        expect(() => {
            checkIfUserIsAlreadyVerified(unverifiedUser);
        }).not.toThrow();
    });
});