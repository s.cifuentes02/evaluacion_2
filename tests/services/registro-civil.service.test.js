import { getCriminalRecords } from "../../src/services/registro-civil.service";

jest.mock("axios");

describe("getCriminalRecords", () => {
  it("should return criminal records for a valid RUT", async () => {
    const rut = "123456789";
    const responseData = {
      rut,
      fullName: "John Doe",
      quantity: 2,
    };

    // Simulamos la respuesta directamente en la promesa
    const axiosInstance = {
      get: jest.fn().mockResolvedValue({ data: responseData }),
    };

    const result = await getCriminalRecords(rut, axiosInstance);

    expect(result).toEqual(responseData);
    expect(axiosInstance.get).toHaveBeenCalledTimes(1);
    expect(axiosInstance.get).toHaveBeenCalledWith(`/person/${rut}/criminal_records`);
  });

  it("should handle errors", async () => {
    const rut = "123456789";
    const error = new Error("API Error");

    // Simulamos el error directamente en la promesa
    const axiosInstance = {
      get: jest.fn().mockRejectedValue(error),
    };

    await expect(getCriminalRecords(rut, axiosInstance)).rejects.toThrow(error);
    expect(axiosInstance.get).toHaveBeenCalledTimes(1);
    expect(axiosInstance.get).toHaveBeenCalledWith(`/person/${rut}/criminal_records`);
  });
});