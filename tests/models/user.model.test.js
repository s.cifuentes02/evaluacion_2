import mongoose from "mongoose";
import UserModel from "../../src/models/user.model";

describe("User Model", () => {
    beforeAll(async () => {
        // Conectar a la base de datos de prueba
        await mongoose.connect("mongodb://localhost/testdb", {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
    });

    afterAll(async () => {
        // Desconectar de la base de datos de prueba
        await mongoose.connection.close();
    });
    afterEach(async () => {
        // Eliminar todos los registros de usuarios después de cada prueba
        await UserModel.deleteMany({});
    });

    it("should be able to create a new user", async () => {
        const userData = {
            name: "John Doe",
            email: "john@example.com",
            password: "password123",
            rut: "ABCD1234",
            code: null
        };
        
        const newUser = new UserModel(userData);
        const savedUser = await newUser.save();

        expect(savedUser._id).toBeDefined();
        expect(savedUser.name).toBe(userData.name);
        expect(savedUser.email).toBe(userData.email);
        expect(savedUser.password).toBe(savedUser.password);
        expect(savedUser.rut).toBe(userData.rut);
        expect(savedUser.verified).toBeFalsy();
        expect(savedUser.code).toBeNull();
        expect(savedUser.blocked).toBeFalsy();
        expect(savedUser.createdAt).toBeDefined();
        expect(savedUser.updatedAt).toBeDefined();
    });

    it("should require name field", async () => {
        const userData = {
            // name field is missing
            email: "johnn@example.com",
            password: "password123",
            rut: "ABCD1234",
        };

        const newUser = new UserModel(userData);

        try {
            await newUser.save();
        } catch (error) {
            expect(error.name).toBe("ValidationError");
            expect(error.errors.name).toBeDefined();
        }
    });

    it("should require email field", async () => {
        const userData = {
            name: "John Doe",
            // email field is missing
            password: "password123",
            rut: "ABCD1234",
        };

        const newUser = new UserModel(userData);

        try {
            await newUser.save();
        } catch (error) {
            expect(error.name).toBe("ValidationError");
            expect(error.errors.email).toBeDefined();
        }
    });

    it("should require rut field", async () => {
        const userData = {
            name: "John Doe",
            email: "john@example.com",
            password: "password123",
            // rut field is missing
        };

        const newUser = new UserModel(userData);

        try {
            await newUser.save();
        } catch (error) {
            expect(error.name).toBe("ValidationError");
            expect(error.errors.rut).toBeDefined();
        }
    });
});
