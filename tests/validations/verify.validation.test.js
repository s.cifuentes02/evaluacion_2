import Joi from "joi";
import { verifyValidation } from "../../src/validations/verify.validation";
import { HTTPError } from "../../src/helpers/error.helper";
import verifyMessages from "../../src/messages/verify.messages";

describe("Verify Validations", () => {
    const validationErrorName = verifyMessages.validation.name;
    const mockHTTPError = new HTTPError({
        name: validationErrorName,
        msg: verifyMessages.validation.messages.code,
        code: 400,
    });
    const mockStringRequired = jest.fn().mockReturnValue(mockHTTPError);

    beforeAll(() => {
        jest.resetModules();
        jest.mock("../../src/validations/common.validations", () => ({
            stringRequired: mockStringRequired,
        }));
    });

    it("should throw a validation error for invalid data", () => {
        const data = {
            // código faltante, necesario para provocar el error de validación
            // otros campos...
        };

        const { error } = verifyValidation.validate(data);

        expect(error).toBeDefined();
        expect(error.name).toBe(validationErrorName);
        expect(error.message).toBe(""); // Ajustar el mensaje esperado aquí
        expect(error.statusCode).toBe(400);
    });
});