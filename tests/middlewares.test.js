import { returnErrorResponse } from "../src/helpers/error.helper";
import { verifyToken } from "../src/helpers/jwt.helper";

import { authMiddleware } from "../src/middlewares.js";

// Mock returnErrorResponse
jest.mock("../src/helpers/error.helper", () => ({
    returnErrorResponse: jest.fn(),
}));

// Mock verifyToken
jest.mock("../src/helpers/jwt.helper.js", () => ({
    verifyToken: jest.fn(),
}));

describe("Auth Middleware", () => {
    let req;
    let res;
    let next;

    beforeEach(() => {
        req = {
            headers: {
                Authorization: "Bearer <token>",
            },
        };
        res = {
            status: jest.fn().mockReturnThis(),
            send: jest.fn(),
        };
        next = jest.fn();
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it("should return 401 if authorization token is missing", () => {
        req.headers.Authorization = undefined;

        authMiddleware(req, res, next);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.send).toHaveBeenCalledWith({ error: "need an authorization token!" });
        expect(next).not.toHaveBeenCalled();
        expect(returnErrorResponse).not.toHaveBeenCalled();
        expect(verifyToken).not.toHaveBeenCalled();
    });

    it("should call verifyToken and set userId in req if token is valid", () => {
        const payload = { id: "user123" };
        verifyToken.mockReturnValue(payload);

        authMiddleware(req, res, next);

        expect(verifyToken).toHaveBeenCalledWith("<token>");
        expect(req.userId).toBe("user123");
        expect(next).toHaveBeenCalled();
        expect(returnErrorResponse).not.toHaveBeenCalled();
        expect(res.status).not.toHaveBeenCalled();
        expect(res.send).not.toHaveBeenCalled();
    });

    it("should call returnErrorResponse with the error if token verification fails", () => {
        const error = new Error("Token verification failed");
        verifyToken.mockImplementation(() => {
            throw error;
        });

        authMiddleware(req, res, next);

        expect(verifyToken).toHaveBeenCalledWith("<token>");
        expect(returnErrorResponse).toHaveBeenCalledWith(error, res);
        expect(res.status).not.toHaveBeenCalled();
        expect(res.send).not.toHaveBeenCalled();
        expect(next).not.toHaveBeenCalled();
    });
});